var http = require("http");

class UseClass {

  constructor() {
    this.funcArr = [];
  }
  use(func) {
    this.funcArr.push(func);
  }
  start(port, host, callback) {
    http.createServer((request, response) => {
      this.funcArr.forEach(func => {
        func(request, response);
      })
    }).listen(port, host);
    callback();
  }
}

module.exports = UseClass;